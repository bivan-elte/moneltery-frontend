import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  users: User[] = [];
  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
  ) { }

  async ngOnInit() {
    await this.getUsers();
  }

  async getUsers() {
    this.users = await this.userService.getUsers();
  }

  async deleteUser(id: number) {
    await this.userService.deleteUser(id);
    if(this.authService.user.id == id) {
      this.authService.logout();
      this.router.navigate(['/']);
    }
    await this.getUsers();
  }
}
