import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { httpOptions } from "./auth.service";
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  private userUrl = 'http://localhost:8080/users';

  constructor(
    private http: HttpClient
  ) { }

  getUsers(): Promise<User[]> {
    return this.http.get<User[]>(
      this.userUrl,
      httpOptions
    ).toPromise();
  }

  deleteUser(id: number): Promise<void> {
    return this.http.delete<void>(
      `${this.userUrl}/${id}`,
      httpOptions
    ).toPromise();
  }
}
