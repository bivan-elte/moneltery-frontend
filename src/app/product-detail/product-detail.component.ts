import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../product.service';
import { Product } from '../product';
import { AuthService } from '../auth.service';

@Component({
  selector: 'product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  id: number;
  product: Product;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router,
    private productService: ProductService,
  ) { }

  async ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.id = +id;
      this.product = await this.productService.getProduct(this.id);
      console.log(this.product);
    }
  }

  async deleteProduct(id: number) {
    await this.productService.deleteProduct(id);
    this.router.navigate(['/products']);

  }

}