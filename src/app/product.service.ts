import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { httpOptions } from "./auth.service";
import { Product } from './product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private productUrl = 'http://localhost:8080/products';

  constructor(
    private http: HttpClient
  ) { }

  getProducts(): Promise<Product[]> {
    return this.http.get<Product[]>(
      this.productUrl,
      httpOptions
    ).toPromise();
  }

  getProduct(id: number): Promise<Product> {
    return this.http.get<Product>(
      `${this.productUrl}/${id}`,
      httpOptions
    ).toPromise();
  }

  modifyProduct(id: number, product: Product): Promise<Product> {
    return this.http.put<Product>(
      `${this.productUrl}/${id}`,
      product,
      httpOptions
    ).toPromise();
  }

  updateProduct(id: number, product: Product): Promise<Product> {
    return this.http.put<Product>(
      `${this.productUrl}/${id}`,
      product,
      httpOptions
    ).toPromise();
  }

  addProduct(product: Product): Promise<Product> {
    return this.http.post<Product>(
      this.productUrl,
      product,
      httpOptions
    ).toPromise();
  }

  deleteProduct(id: number): Promise<void> {
    return this.http.delete<void>(
      `${this.productUrl}/${id}`,
      httpOptions
    ).toPromise();
  }
}