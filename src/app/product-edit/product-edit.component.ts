import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from "@angular/router";
import { Location } from "@angular/common";
import { Product } from "../product";
import { ProductService } from "../product.service";
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {

  product: Product = new Product();
  id: number = null;
  title = 'New product'

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private location: Location
  ) { }

  async ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.id = +id;
      this.product = await this.productService.getProduct(this.id);
      this.title = 'Edit product';
    }
    // this.route.paramMap
    // this.route.params.pipe(switchMap(async (params: ParamMap) => {
    //   const id = params.get('id');
    //   this.product = id !== null 
    //     ? await this.productService.getProduct(+id)
    //     : new Product();
    //   return of({});
    // }))
    // .subscribe();
  }

  async onFormSubmit(product: Product) {
    console.log("Itt?", product);
    if (this.id > 0) {
      console.log("Product updated");
      await this.productService.updateProduct(product.id, product);   
    } else {
      console.log("Product added");
      await this.productService.addProduct(product);
    }
    this.location.back();
  }

}
