import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  products: Product[] = []
  filteredProducts = [];
  selectedProduct = '';

  constructor(
    private productService: ProductService
  ) { }

  async ngOnInit() {
    this.products = await this.productService.getProducts();
    this.filterProducts();
  }

  filterProducts() {
    this.filteredProducts = this.selectedProduct === ''
      ? this.products
      : this.products.filter(product => product.category === this.selectedProduct);
  }

  onFilterChange(data) {
    this.selectedProduct = data;
    this.filterProducts();
  }

}