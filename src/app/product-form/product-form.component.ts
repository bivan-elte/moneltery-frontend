import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Product } from '../product';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit, OnChanges {

  productForm = this.fb.group({
    name: ['', [Validators.required]],
    category: ['', [Validators.required]],
    amount: ['', [Validators.required]],
    price: ['', [Validators.required]],

  });
  @Input() product: Product;
  @Output() save = new EventEmitter<Product>();

  get name() { return this.productForm.get('name'); }
  get category() { return this.productForm.get('category'); }
  get amount() { return this.productForm.get('amount'); }
  get price() { return this.productForm.get('price'); }


  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private fb: FormBuilder
  ) { }

  ngOnInit() { }

  ngOnChanges() {
    this.productForm.patchValue(this.product);
  }

  async onSubmit() {
    const routeId = this.route.snapshot.paramMap.get('id');
    this.productForm.value.id = routeId;
    this.productForm.value.createdAt = new Date();
    const obj = Object.assign(new Product(), this.productForm.value);
    if(!routeId) await this.productService.addProduct(obj);
    this.save.emit(obj);
  }

}