# MonelteryFrontend

#### Szükséges legalább a Node.js 8.0 és NPM 5.2.
A következő utasításokkal ellenőrizhetjük a telepített verziót:
```
> node -v 
v10.11.0  
> npm -v 
6.4.1  
```
Node.js letölthető [innen](https://nodejs.org/en/download/), tartalmazza az NPM-et is.



### Ajánlott IDE: Visual Studio Code
Letölthető itt: [Visual Studio Code](https://code.visualstudio.com/download) , kövessük a letöltési útmutatót.

### Futtatás
1. Kattintson a **"..."** gombra, azon belül kattintson a **"Download repository"** lehetőségre.
2. A letöltött állományt csomagoljuk ki.
3. A kicsomagolt állomány mappájában adjuk ki a **`ng serve`** parancsot.
4. Sikeres futtatás estén a `http://localhost:4200`-on elinful a program.

### Könyvtárstruktúra
- e2e
- node_modules
- src
	- app
		- `login` (a bejelentkezési felületért, logikáért felel)
		- `main-page` (a főoldal megjelenítését biztosítja)
		- `product-detail` (egy termékre kattintva megjeleníti annak részletes leírását)
		- `product-edit` (egy kiválasztott termék adatait felülírhatjuk)
		- `product-form` (a termék adataitnak sablonját jeleníti meg szerkesztés, új termék hozzáadása közben, ellenőrzi kitöltöttségüket)
		- `product-list` (a termékek listázását végzi)
		- `routing` (a megfelelő "path" összekötése a megfelelő komponenssel)
		- `status-filter` (a listázott termékek kategória szerinti szűkítését végzi)
		- `user-list` (a felhasználók listázását végzi, itt lehet felhasználókat törölni is)
		- ...
	- ...
- ...